1.Download the Script to local bin folder
`sudo wget https://gitlab.com/crazyelmo/unifibackuptonas/-/blob/master/unifi_backup.sh -O /usr/local/bin/unifi_backup.sh`

2.Change permisison to Script for execution rights
`sudo chmod +x /usr/local/bin/unifi_backup.sh`

3. Modify the Script Variable
`sudo nano -w /usr/local/bin/unifi_ssl_import.sh`

#Find variable and update nas location
`nas='192.168.1.16:/volume1/linux'`


4. schedule the script to run every 12hrs
`sudo nano -w /etc/crontab`

Now, add the following two lines to the end of the file:
`5 */12 * * * root unifi_backup.sh`
