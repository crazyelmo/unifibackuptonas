#!/bin/bash

# Path to where unifi saves all backups
unifipath='/usr/lib/unifi/data/backup/'

#CustomVar
nas='192.168.1.16:/volume1/linux'
mountpath='/mnt/nasmount'

#Script Var
datestamp="$(date +"%d-%m-%Y")"
backuppath="${mountpath}/backups/unifi/${datestamp}"


# MAIN CODE ##############

echo "Map Backup Drive"
sudo rm -d $mountpath
sudo mkdir $mountpath
sudo mount $nas $mountpath

echo "Create Backup Directory"
mkdir -p $backuppath

echo 'backup unifi controller'
sudo rsync -av $unifipath $backuppath

echo 'change permissions of all files (because some of them are owned by root)'
sudo find $backuppath -exec chown -R pi:pi {} \;